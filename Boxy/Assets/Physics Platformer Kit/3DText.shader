﻿Shader "GUI/3DText Shader - Cull Back"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_Color ("Text Color", Color) = (1, 1, 1, 1)
    }
    SubShader
    {
        // No culling or depth
        Lighting Off Cull Back ZWrite Off Fog { Mode Off }

        Pass
        {
			Color [_Color]
			SetTexture[_MainTex] {
				combine primary, texture * primary
			}
        }
    }
}
