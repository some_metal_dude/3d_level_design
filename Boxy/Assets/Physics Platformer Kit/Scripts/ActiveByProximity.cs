﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class ActiveByProximity : MonoBehaviour
{
    BoxCollider boxCollider;
    TextMesh textMesh;
    private string displayingText;
    void Start()
    {
        boxCollider = GetComponent<BoxCollider>();
        boxCollider.isTrigger = true;

        textMesh = GetComponent<TextMesh>();
        displayingText = textMesh.text;
        textMesh.text = null;
    }
    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            textMesh.text = displayingText;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            textMesh.text = null;
        }
    }
}
