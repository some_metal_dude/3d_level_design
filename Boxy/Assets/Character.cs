﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


[System.Serializable]
public class CollisionEvent : UnityEvent { }

public class Character : MonoBehaviour
{
    [SerializeField] private float FreezeDuration;
    private Coroutine timerCoroutine;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            Freeze();

            timerCoroutine = GetComponent<TimerTest>().StartTimer(FreezeDuration, Unfreeze);
        }

        if (Input.GetButtonDown("Fire1"))
        {
            GetComponent<TimerTest>().StopTimer(timerCoroutine);
        }
    }

    void Freeze()
    {

    }

    void Unfreeze()
    {

    }
}
