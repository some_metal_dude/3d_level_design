﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerTest : MonoBehaviour
{
    public delegate void TimerCallback();
    public Coroutine StartTimer(float duration, TimerCallback onComplete)
    {
        return StartCoroutine(TimerCoroutine(duration, onComplete));
    }

    public void StopTimer(Coroutine coroutine)
    {
        StopCoroutine(coroutine);
    }

    private IEnumerator TimerCoroutine(float duration, TimerCallback onComplete)
    {
        yield return new WaitForSeconds(duration);

        onComplete.Invoke();
    }
}
